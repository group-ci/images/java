FROM registry.gitlab.com/group-ci/images/centos:8_2020_12
COPY maven_settings.xml /
RUN yum install which java-11-devel -y && \
    yum clean all
ENV JAVA_HOME=/usr/lib/jvm/java-11-openjdk
